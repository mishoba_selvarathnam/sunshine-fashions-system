<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class AdminSeeder extends Seeder
{
    public function run()
    {
        $user = User::create([
            'username' => 'Admin',
            'role' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('password')

        ]);

        $user = User::create([
            'username' => 'User',
            'role' => 'User',
            'email' => 'misho@gmail.com',
            'password' => bcrypt('password')

        ]);
    }
}
