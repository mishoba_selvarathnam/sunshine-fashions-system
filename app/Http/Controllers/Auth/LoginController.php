<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    use AuthenticatesUsers;

    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user) {
        if (Auth::user()->isAdmin())  {
            toastr()->success('Welcome to admin Dashboard!');
            return redirect('/admin');
        } else  {
            toastr()->success('Welcome back to 2k Fashions!');
            return redirect('/');
        }
    }
}
