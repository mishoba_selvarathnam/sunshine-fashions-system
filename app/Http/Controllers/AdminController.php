<?php

namespace App\Http\Controllers;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use Notification;
use App\Notifications\SunshineFashoins;

class AdminController extends Controller
{
    public function index()
    {
        return view('backend.admin.home');
    }

    public function pendingItems()
    {
        $data = Order::where('status', 'pending')->get();
        return view('backend.order.pending', compact('data'));
    }

    public function deliveredItems()
    {
        $data = Order::where('status', 'completed')->get();
        return view('backend.order.delivered', compact('data'));
    }

    public function show($id)
    {
        $order = Order::find($id);
        $items = OrderItem::where('order_id', $order->id)->get();
        return view('backend.order.showItem', compact('order', 'items'));
    }

    public function update_status(Request $request, $id)
    {
        $this->validate($request,[
            'status' => 'required'
        ]);

        $input = $request->all();


        $order = Order::find($id);
        $order->update($input);

        

        echo "success";

        $notification = array(
            'message' => 'Order Marked as Delivered successfully!', 
            'alert-type' => 'success'
        );

        toastr()->success('Order Details Updated Successfully!');

        return redirect()->back()
                    ->with($notification);

    }
}
