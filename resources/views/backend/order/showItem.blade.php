<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>2k Fashions | Admin</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{asset('admin_dashboard/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('admin_dashboard/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <link rel="stylesheet" href="{{asset('admin_dashboard/bower_components/Ionicons/css/ionicons.min.css')}}">
  <link rel="stylesheet" href="{{asset('admin_dashboard/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
  <link rel="stylesheet" href="{{asset('admin_dashboard/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
  <link rel="stylesheet" href="{{asset('admin_dashboard/plugins/iCheck/all.css')}}">
  <link rel="stylesheet" href="{{asset('admin_dashboard/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css')}}">
  <link rel="stylesheet" href="{{asset('admin_dashboard/plugins/timepicker/bootstrap-timepicker.min.css')}}">
  <link rel="stylesheet" href="{{asset('admin_dashboard/bower_components/select2/dist/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('admin_dashboard/dist/css/AdminLTE.min.css')}}">
  <link rel="stylesheet" href="{{asset('admin_dashboard/dist/css/skins/_all-skins.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/card.css')}}">

  
  <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue fixed sidebar-mini">
<div class="wrapper">

<header class="main-header">
    <a href="/admin" class="logo">
      <span class="logo-lg"><b>2k</b> Fashions</span>
    </a>

    <nav class="navbar navbar-static-top">

      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">


          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{asset('admin_dashboard/dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{ Auth::user()->username }}</span>
            </a>
            <ul class="dropdown-menu">

              <li class="user-header">
                <img src="{{asset('admin_dashboard/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">

                <p>
                {{ Auth::user()->username }} - {{ Auth::user()->role }}
                  <small>Member since {{ Auth::user()->created_at }}</small>
                </p>
              </li>

              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="{{ route('logout') }}" class="btn btn-default btn-flat" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Sign out') }}</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
</header>

  <aside class="main-sidebar">

    <section class="sidebar">

      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset('admin_dashboard/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->username}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>

      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Top Menu</li>

        <li><a href="{{route('user.index')}}"><i class="fa fa-user"></i> <span>User List</span></a></li>
        <li><a href="{{route('category.index')}}"><i class="fa fa-book"></i> <span>Categories</span></a></li>
        <li><a href="{{route('product.index')}}"><i class="fa fa-list-alt"></i> <span>Items</span></a></li>
        <li><a href="{{route('pendingItems')}}"><i class="fa fa-list-alt"></i> <span>Pending Orders</span></a></li>
        <li><a href="{{route('deliveredItems')}}"><i class="fa fa-list-alt"></i> <span>Delivered Orders</span></a></li>
    </section>
  </aside>

  <div class="content-wrapper">

    <section class="content-header">
      <h1>
      @if($order->status == 'pending')
        Pending Items
      @else
        Delivered items
      @endif
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Showing Items</li>
      </ol>
    </section>

    <section class="content">
  

      <div class="row">

        <section class="col-lg-12  col-md-12 col-sm-12">
        

            <div class="box">

            
            @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
              <div class="row justify-content-between" style="margin-top:20px;">
              @if($order->status == 'pending')
                <div class="col-md-5" style="margin-left:20px;">
                    <a class="btn btn-primary" href="{{route('pendingItems')}}"> Back</a>
                </div>
                
                  
                <div class="col-md-6" style="text-align:right;">
                <form method="post" action="{{ route('showItem.update_status',$order->id) }}" enctype="multipart/form-data">
                  @csrf
                  @method('patch')
                    <input type="text" id="status" name="status" value="completed" style="display:none;">
                    <button type="submit" class="btn btn-success">Mark as Delivered</button>
                </div>
                </form>
                @else
                <div class="col-md-5" style="margin-left:20px;">
                    <a class="btn btn-primary" href="{{route('deliveredItems')}}"> Back</a>
                </div>
                @endif
              </div>

              

              <div class="row" style="margin-top:20px;">
                <div class="row">
                <h4 class="text-center" style="font-size:20px; align:center;">Order Details</h4>
                </div>
                <hr style="width:1000px;">
                <div class="col-md-1"></div>
                <div class="col-md-2" style="font-size:20px;">
                  Order Number: 
                </div>
                <div class="col-md-5" style="font-size:20px;">{{ $order->order_number }}</div>
              </div>

              <div class="row" style="margin-top:10px;">
                <div class="col-md-1"></div>
                <div class="col-md-2" style="font-size:20px;">
                  Total amount : 
                </div>
                <div class="col-md-5" style="font-size:20px;">Rs. {{ $order->grand_total }}/=</div>
              </div>

              <div class="row" style="margin-top:10px;">
                <div class="col-md-1"></div>
                <div class="col-md-2" style="font-size:20px;">
                  Total Order Count: 
                </div>
                <div class="col-md-5" style="font-size:20px;">{{ $order->item_count }}</div>
              </div>

              <div class="row" style="margin-top:10px;">
                <div class="col-md-1"></div>
                <div class="col-md-2" style="font-size:20px;">
                  Payment Method: 
                </div>
                <div class="col-md-5" style="font-size:20px;">{{ $order->payment_method }}</div>
              </div>

              <div class="row" style="margin-top:10px;">
                <div class="col-md-1"></div>
                <div class="col-md-2" style="font-size:20px;">
                  Payment Method: 
                </div>
                <div class="col-md-5" style="font-size:20px;">
                  @if($order->payment_status == 1)
                    <p>Payment Done</p>
                  @else
                  <p>Payment Not Done</p>
                  @endif
                </div>
              </div>

              <div class="row" style="margin-top:10px;">
                <div class="col-md-1"></div>
                <div class="col-md-2" style="font-size:20px;">
                  Order Status: 
                </div>
                <div class="col-md-5" style="font-size:20px;">
                  @if($order->status == 'completed')
                    <p>Order Delivered</p>
                  @else
                  <p>Pending to Order</p>
                  @endif
                <br><br></div>
              </div>
              
                <div class="row" style="margin-top:10px;">
                  <div class="col-md-1"></div>
                  <div class="col-md-1" style="font-size:20px;">
                    Items: 
                  </div>
                </div>
                <div class="row">
                  @foreach($items as $item)
                  
                    <div class="col-md-3" style="font-size:20px;">
                      <img src="/photo/{{ $item->avatar }}" style="height:300px; width:300px; " alt="">
                    </div>

                    <div class="col-md-3">
                      <p style="font-size:20px; margin-top:10px;">Quantity:&nbsp; &nbsp; &nbsp; {{ $item->quantity }}</p>
                      <p style="font-size:20px; margin-top:10px;">Unit Price: Rs. {{ $item->price }}/=</p>
                      <p style="font-size:20px; margin-top:10px;">Total Price: Rs. {{ $item->price * $item->quantity }}/=</p>
                    </div>
                  @endforeach
                </div>

                <h4 class="text-center" style="font-size:20px; align:center;">Details of the Person</h4>
                  <hr width="1000px">

                  <div class="row" style="margin-top:20px;">
                    <div class="col-md-1"></div>
                    <div class="col-md-2" style="font-size:20px;">
                      Full Name : 
                    </div>
                    <div class="col-md-5" style="font-size:20px;">{{ $order->firstname }} {{ $order->lastname }}</div>
                  </div>

                  <div class="row" style="margin-top:10px;">
                    <div class="col-md-1"></div>
                    <div class="col-md-2" style="font-size:20px;">
                      Address : 
                    </div>
                    <div class="col-md-5" style="font-size:20px;">{{ $order->line1 }}, {{ $order->line2 }} <br>{{ $order->post_code }}.</div>
                  </div>

                  <div class="row" style="margin-top:10px;">
                    <div class="col-md-1"></div>
                    <div class="col-md-2" style="font-size:20px;">
                      Phone Number : 
                    </div>
                    <div class="col-md-5" style="font-size:20px;">{{ $order->phone }}
                    <br><br>
                    </div>
                  </div>

            </div>
        </section>
      </div>

    </section>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright <a>2k Fashions</a>.</strong> All rights
    reserved.
  </footer>
</div>

<script src="{{asset('admin_dashboard/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('admin_dashboard/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('admin_dashboard/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('admin_dashboard/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{asset('admin_dashboard/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{asset('admin_dashboard/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
<script src="{{asset('admin_dashboard/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('admin_dashboard/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('admin_dashboard/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('admin_dashboard/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
<script src="{{asset('admin_dashboard/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
<script src="{{asset('admin_dashboard/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('admin_dashboard/plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{asset('admin_dashboard/bower_components/fastclick/lib/fastclick.js')}}"></script>
<script src="{{asset('admin_dashboard/dist/js/adminlte.min.js')}}"></script>
<script src="{{asset('admin_dashboard/dist/js/demo.js')}}"></script>

</body>
</html>
