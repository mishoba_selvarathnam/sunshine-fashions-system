@extends('frontend.layouts.main')

@section('title')
Register
@endsection

@section('content')

<div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Register</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="single-product-area">
        <div class="zigzag-bottom"></div>
        <div class="container">

            <div class="row">
                <div class="col-md-11">
                    <div class="single-sidebar">
                        <div id="login-form-wrap" class="login" method="post">
                            <p>If you are a member with us before, please login <a href="{{route('login')}}">Login to continue...</a> If you are a new customer please proceed to the Register to join with us.</p>
                        </div>
                    </div>
                </div>
            </div>

            @if (count($errors) > 0)
            <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                            
                        <div class="col-md-8">
                            <div class="single-sidebar">
                                <h2 class="sidebar-title">Register Here...</h2>
                                <form method="POST" id="login-form-wrap" action="{{ route('register') }}">
                                @csrf
                                <input type='text' name= role id ="userN" value="User" style="display:none;">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="userN">User Name <span>*</span></label>
                                            <input type='text' name= username id ="userN" placeholder="Enter the user name.."><br>
                                        </div>

                                        <div class="col-md-4">
                                            <label for="email">E-mail Address <span>*</span></label>
                                            <input type="email" name="email" id="email"  style="width:250px;" placeholder="type a valid email">
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-md-6">
                                            <br>
                                            <label for="pwd">Password <span>*</span></label>
                                            <input type="password" name="password" id='pwd' placeholder="type a complex password">
                                            
                                        </div>

                                        <div class="col-md-4">
                                            <br>
                                            <label for="pwd">Confirm Password <span>*</span></label>
                                            <input type="password" name="password_confirmation" id='pwd' placeholder="Re-type the password">
                                        </div>

                                        <div class="col-md-12">
                                        <br>
                                            <button type='submit' class="btn btn-success">{{ __('Register') }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection