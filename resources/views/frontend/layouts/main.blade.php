<!DOCTYPE html>
<!--
	ustora by freshdesignweb.com
	Twitter: https://twitter.com/freshdesignweb
	URL: https://www.freshdesignweb.com/ustora/
-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>2k Fashions | @yield('title')</title>
    
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('store/css/bootstrap.min.css')}}">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('store/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('store/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('store/style.css')}}">
    <link rel="stylesheet" href="{{asset('store/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/footer.css')}}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />

    @yield('css')
    @toastr_css

  </head>
  <body>
   
    <div class="header-area">
        <div class="container">
            <div class="row">
                
                <div class="col-md-11">
                    <div class="header-right">
                        <ul class="list-unstyled list-inline">

                            @guest
                                <li><a href="{{route('login')}}">Login</a></li>
                                @if(Route::has('register'))
                                    <li><a href="{{route('register')}}">Register</a></li>
                                @endif

                                @else
                                <li>
                                    <i class="fa fa-user"></i> 
                                    {{ Auth::user()->username }}
                                </li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </li>
                            @endguest

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End header area -->

    <div class="site-branding-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="logo">
                        
                    </div>
                </div>
                
                <div class="col-sm-5">
                    <div class="dropdown">
                        <button type="button" class="btn btn-info" data-toggle="dropdown">
                            <i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart <span class="badge badge-pill badge-danger">{{ count((array) session('cart')) }}</span>
                        </button>
                        <div class="dropdown-menu">
                            <div class="row total-header-section">
                                <div class="col-lg-6 col-sm-6 col-6">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="badge badge-pill badge-danger">{{ count((array) session('cart')) }}</span>
                                </div>

                                <?php $total = 0 ?>
                                @foreach((array) session('cart') as $id => $details)
                                    <?php $total += ($details['price'] * $details['quantity']) ?>
                                @endforeach

                                <div class="col-lg-6 col-sm-6 col-6 total-section text-right">
                                    <p>Total: <span class="text-info">Rs. {{ $total }}.00</span></p>
                                </div>
                            </div>

                            @if(session('cart'))
                                @foreach(session('cart') as $id => $details)
                                    <div class="row cart-detail">
                                        <div class="col-lg-4 col-sm-4 col-4 cart-detail-img">
                                            <img src="/photo/{{ $details['photo'] }}">
                                        </div>
                                        <div class="col-lg-8 col-sm-8 col-8 cart-detail-product">
                                            <p>{{ $details['name'] }}</p>
                                            <span class="price text-info">Rs. {{ $details['price'] }}.00</span> <span class="count"> Quantity:{{ $details['quantity'] }}</span>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="{{ url('cart') }}" class="btn btn-primary btn-block" style="font-size:18px;">View all</a>
                                </div>
                                <div class="col-md-6">
                                    <a href="{{ url('checkout') }}" class="btn btn-primary btn-block" style="font-size:18px;">Checkout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <br>
    </div>

    
    <div class="mainmenu-area">
        <div class="container">
            <div class="row">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div> 
                <div class="navbar-collapse collapse">
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="nav navbar-nav">
                                <li><img src="{{asset('store/img/logo.png')}}" height="80" width="100">2k Fashions</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                        <ul class="nav navbar-nav">
                        
                        <li class="active"><a href="{{route('welcome')}}">Home</a></li>
                        <li><a href="{{route('about')}}">About</a></li>
                        <li><a href="{{route('products')}}">Products</a></li>
                        <li><a href="{{route('order.index')}}">Checkout</a></li>
                        <li><a href="{{route('contact')}}">Contact</a></li>
                    </ul>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>

    @if(session('success'))

        <div class="alert alert-success">
            {{ session('success') }}
        </div>

    @endif
    
     @yield('content')
    
     <div class="mt-5 pt-5 pb-5 footer">
        <div class="container">
            <div class="row">
                <br><br><br>
                <div class="col-lg-5 col-xs-12 about-company">
                    <h2>2k Fashions</h2>
                    <p class="pr-5 text-white-50">2k Fashions offers a wide range of clothing to fit todays Sri Lankan customer’s unique sense of style. Our clothing and accessories are carefully curated to provide our customers the latest fashions. </p>
                </div>
                <div class="col-lg-3 col-xs-12 links">
                    <h4 class="mt-lg-0 mt-sm-3">Useful Links</h4>
                        <ul class="m-0 p-0">
                        <li><a href="{{route('welcome')}}">Home</a></li>
                        <li><a href="{{route('about')}}">About</a></li>
                        <li><a href="{{route('products')}}">Products</a></li>
                        <li><a href="{{route('order.index')}}">Checkout</a></li>
                        <li><a href="{{route('contact')}}">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-4 col-xs-12 location">
                    <h4 class="mt-lg-0 mt-sm-4">Contact</h4>
                    <p>N0.44, Point pedro Road, Jaffna.</p>
                    <p class="mb-0"><i class="fa fa-phone mr-3"></i>&nbsp;<a href="tel:(+94) 77 345 7865">(+94) 77 345 7865</a></p>
                    <p class="mb-0"><i class="fa fa-phone mr-3"></i>&nbsp;<a href="tel:(+94) 21 403 6754 ">(+94) 21 403 6754</a></p>
                    <p><i class="fa fa-envelope-o mr-3"></i>&nbsp; <a href="mailto:fashions2ks@gmail.com">fashions2ks@gmail.com</a></p>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col copyright">
                    <p class=""><small class="text-white-50">© 2019. All Rights Reserved.</small></p>
                </div>
            </div>
        </div>
    
    </div> <!-- End footer top area -->
        @jquery
        @toastr_js
        @toastr_render
   
    <!-- Latest jQuery form server -->
    <script src="https://code.jquery.com/jquery.min.js"></script>
    
    <!-- Bootstrap JS form CDN -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    
    <!-- jQuery sticky menu -->
    <script src="{{asset('store/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('store/js/jquery.sticky.js')}}"></script>
    
    <!-- jQuery easing -->
    <script src="{{asset('store/js/jquery.easing.1.3.min.js')}}"></script>
    
    <!-- Main Script -->
    <script src="{{asset('store/js/main.js')}}"></script>
    
    <!-- Slider -->
    <script type="text/javascript" src="{{asset('store/js/bxslider.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('store/js/script.slider.js')}}"></script>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

  
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

  

<script type="text/javascript">

$(function() {

    var $form         = $(".require-validation");

  $('form.require-validation').bind('submit', function(e) {

    var $form         = $(".require-validation"),

        inputSelector = ['input[type=email]', 'input[type=password]',

                         'input[type=text]', 'input[type=file]',

                         'textarea'].join(', '),

        $inputs       = $form.find('.required').find(inputSelector),

        $errorMessage = $form.find('div.error'),

        valid         = true;

        $errorMessage.addClass('hide');

 

        $('.has-error').removeClass('has-error');

    $inputs.each(function(i, el) {

      var $input = $(el);

      if ($input.val() === '') {

        $input.parent().addClass('has-error');

        $errorMessage.removeClass('hide');

        e.preventDefault();

      }

    });

  

    if (!$form.data('cc-on-file')) {

      e.preventDefault();

      Stripe.setPublishableKey($form.data('stripe-publishable-key'));

      Stripe.createToken({

        number: $('.card-number').val(),

        cvc: $('.card-cvc').val(),

        exp_month: $('.card-expiry-month').val(),

        exp_year: $('.card-expiry-year').val()

      }, stripeResponseHandler);

    }

  

  });

  

  function stripeResponseHandler(status, response) {

        if (response.error) {

            $('.error')

                .removeClass('hide')

                .find('.alert')

                .text(response.error.message);

        } else {

            // token contains id, last4, and card type

            var token = response['id'];

            // insert the token into the form so it gets submitted to the server

            $form.find('input[type=text]').empty();

            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");

            $form.get(0).submit();

        }

    }

  

});

</script>

    @yield('script')

    <script>
        @if(Session::has('message'))
            var type="{{Session::get('alert-type','info')}}"

            switch(type){
                case 'info':
                    toastr.info("{{ Session::get('message') }}");
                    break;
                case 'success':
                    toastr.success("{{ Session::get('message') }}");
                    break;
                case 'warning':
                    toastr.warning("{{ Session::get('message') }}");
                    break;
                case 'error':
                    toastr.error("{{ Session::get('message') }}");
                    break;
            }
        @endif
    </script>


    <script>
    function onClick(element) {
    document.getElementById("img01").src = element.src;
    document.getElementById("modal01").style.display = "block";
    }
</script>

  </body>
</html>