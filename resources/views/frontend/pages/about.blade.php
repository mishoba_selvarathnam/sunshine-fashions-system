@extends('frontend.layouts.main')

@section('title')
    About
@endsection

@section('content')

    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>About US</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="single-product-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                        <h2 class="sidebar-title"><center>About us</center></h2>
                            <p>2k Fashions is one of the leading online shopping stores for clothing & fashion stores in Sri Lanka. 
                            We are specialized in offering the highest quality women’s clothing at best prices in Sri Lanka. 
                            Our designs are truly unique, comfortable and affordable. 
                            We are specialized for women’s dresses, tops, blouses, skirts, sarees, party frocks and ladies office wear 

                            On the other hand, 2k Fashions is a reliable online platform for thousands of small & medium entrepreneurs in the fashion industry. 
                            We provide them the opportunity to market their products to Sri Lanka’s largest online clothing & fashion customer base. We offer peace 
                            of mind to manufactures & small time garment producers to focus on their production process. 2k Fashions provides them the highly 
                            professional marketing platform to market their products.</p><br><hr>
                </div>

                <div class="col-md-12">
                    <div class="single-sidebar" style="align:center;">
                        <h2 class="sidebar-title"><center>Our Vision</center></h2>
                            <p>Our Vision is to provide trendy & high-quality clothing to ladies in Sri Lanka at the best 
                            reasonable price in the market and create an extraordinary experience to our consumers as well as our suppliers. 
                            Based on the enormous success during our last few years, we decided to make aware of our clothes to 
                            everyone around Sri Lanka by introducing the online facility. The www.2kfashions.com is the site that emerged 
                            for dealing exclusively in trendy women dresses in Sri Lanka. 
                            Join us to become a part of an era, get yourself update with the latest trendy collection of clothing & fashion 
                            products in Sri Lanka and make your wardrobe full of fashionable items with 2kfashions.com. We continue to 
                            power ahead with your support! Keep shopping!!</p><br><hr>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="single-sidebar" style="align:center;">
                        <h2 class="sidebar-title"><center>Wide Range of Styles</center></h2>
                            <p>2k Fashions offers a wide range of clothing to fit today's Sri Lankan customer’s unique sense of style. 
                            Our clothing and accessories are carefully curated to provide our customers with the latest fashions. 
                            To keep our customers in style we post new arrivals daily, as well as offer stylist picks to help any indecisive shoppers. 
                            Beyond helping you look your best, Sunshine Fashions strives to make every purchase a positive experience. 
                            Our top priorities are excellent customer service, exceptionally quick order processing, ultra-fast delivery times, 
                            and hassle-free return policy. We value your feedback, whether positive or constructive and we are continuously 
                            working to improve your experience.

                            If you are a first-time visitor or long-standing customer, we hope you will be thrilled with every aspect of 
                            your online shopping experience with 2k Fashions.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection