@extends('frontend.layouts.main')

@section('title')
    Payment
@endsection

@section('content')

    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Payment</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="single-product-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="container" style="margin-top: 25px;width: 90%;">
                    <div class="container-fluid">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <center>
                                    <h4>You can pay with credit card! <i class="fa fa-smile-o"></i></h4>

                                    <p>You can fill your credit card details on the next screen once you procedd to the order.</p>
                                </center>

                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <table class="shop_table">
                                            <caption>Your Order <br><br></caption>
                                            
                                                <tr>
                                                    <th>Order Number</th>
                                                    <td>{{ $orders->order_number }}</td>
                                                </tr>

                                                <tr>
                                                    <th>Total Number of items</th>
                                                    <td>{{ $orders->item_count }}</td>
                                                </tr>

                                                <tr>
                                                    <th>Total Price</th>
                                                    <td>Rs. {{ $orders->grand_total }}.00</td>
                                                </tr>

                                        </table>
                                    </div>

                                    <div class="col-md-6">
                                        <table class="shop_table" >
                                            <caption>Order Reciver Details <br><br></caption>

                                            <tr>
                                                <th>Full Name</th>
                                                <td>{{ $orders->firstname }} {{ $orders->lastname }}</td>
                                            </tr>

                                            <tr>
                                                <th>Address</th>
                                                <td>{{ $orders->line1 }} {{ $orders->line2 }}</td>
                                            </tr>

                                            <tr>
                                                <th>Postal Code</th>
                                                <td>{{ $orders->post_code }}</td>
                                            </tr>

                                            <tr>
                                                <th>Phone Number</th>
                                                <td>{{ $orders->phone }}</td>
                                            </tr>

                                        </table>
                                    </div>
                                </div>


                                <h2>Your Order Items</h2>

                                    <table class="shop_table" style="width:80%;">
                                        <thead>
                                            <tr>
                                                <td>Picture</td>
                                                <td>Qunatity</td>
                                                <td>Price</td>
                                                <td>Total</td>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach ($item as $item)
                                            <tr class="cart_item">
                                                <td><img src="/photo/{{ $item->avatar }}" alt="" style="height:200; width:200;"></td>
                                                <td>{{ $item->quantity }}</td>
                                                <td>Rs.{{  $item->price }}.00</td>
                                                <td>Rs. {{ $item->price * $item->quantity }}.00</td>
                                            </tr>
                                            @endforeach
                                        </tbody>

                                    </table>

                                    <div class="form-row place-order">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <a class="add_to_cart_button" data-quantity="1" data-product_sku="" data-product_id="70" rel="nofollow" href="{{ route('order.creditcard', $orders->id) }}">Proceed to Order</a>
                                            </div>

                                            <div class="col-md-6">
                                                {!! Form::open(['method' => 'DELETE','route' => ['order.remove', $orders->id],'style'=>'display:inline']) !!}
                                                    {!! Form::button('Cancel Order', ['class'=>'btn btn-danger', 'type'=>'submit']) !!}
                                                    {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
<script
    src="https://www.paypal.com/sdk/js?client-id=SB_CLIENT_ID"> // Required. 
     Replace SB_CLIENT_ID with your sandbox client ID.
  </script>

<script>
    paypal.Buttons().render('#paypal-button-container');
    // This function displays Smart Payment Buttons on your web page.
</script>
@endsection